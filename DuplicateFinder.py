"""
DuplicateFinder.py
Cycles through a given directory and tries to identify duplicate files
Compares first by file size, then by hash value, then moves out any duplicates.

Author: Richard Harrington
Created: 10/12/2013
Last updated: 10/13/2013
"""

import os
import hashlib
import sqlite3
import shutil

dbname='files.db'
filetypes=('jpg','tiff','mpg','wmv','mp4','mp3','mov','avi','3gp','epub','mobi','pdf')

m=hashlib.md5()
scanpath=input("Please enter a folder to scan:\n")
duplicates_dir=input("Enter a destination to move duplicates to:\n")

def write_to_db(filename,filesize,filehash):
    row=(filename,filesize,filehash)
    try:
        con=sqlite3.connect(dbname)
        c=con.cursor()
        c.execute("insert into files (file_name,file_size,hash) values (?,?,?)",row)
        con.commit()
        return True
    except sqlite3.Error as e:
        if con:
            con.rollback()
        print ("Error %s:" % e.args[0])
    finally:
        if con:
            con.close()
def check_db(filename="",filesize="",filehash=""):
    
    try:
        con=sqlite3.connect(dbname)
        c=con.cursor()
        c.execute("select * from files where file_name='"+filename+"' or file_size='"+str(filesize)+"' or hash='"+filehash+"'")
        for result in c.fetchall():
            return result
    except sqlite3.Error as e:
        print ("Error %s:" % e.args[0])
    finally:
        if con:
            con.close()

for dirpath,dirs,files in os.walk(scanpath):
    print ("starting in",dirpath)
    count=0
    dupes=0
    for file in files:
        if file.split('.')[len(file.split('.'))-1].lower() in filetypes:
            filepath=dirpath+"\\"+file
            stats=os.stat(filepath)
            f_size=stats.st_size
            
            if check_db(filesize=f_size) is None:
                with open(filepath,'rb') as fh:
                    while True:
                        data=fh.read(8192)
                        if not data:
                            break
                        m.update(data)
                    hashval=m.hexdigest()
                # filecheck=check_db(filename=file)
                print(hashval)            
                hashcheck=check_db(filehash=hashval)
                if hashcheck is None:
                    write_to_db(file,f_size,hashval)
                    #print("New file", file)
                    count+=1
            else:
            #print("Hash value match found for file: "+filepath)
                try:
                    shutil.move(filepath,duplicates_dir)
                    dupes+=1
                except shutil.Error as e:
                    print(e)


    print("New files found:",count)
    print("Duplicates found:",dupes)
